<?php
    $defaultOptions = array(
        'remoteURL' => false,
        'localDomain' => false,
        'css' => true,
        'js' => false
    );
    
    $options = array_merge($defaultOptions, $_GET);
    $finalURL = false;
    $remoteDomain = false;
    $errors = array();

    extract($options);

    if($remoteURL && $localDomain) {
        $ch = curl_init(); 
    
        curl_setopt($ch, CURLOPT_URL,            $remoteURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT,        15);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
                    
        $data = curl_exec($ch);        
        $finalURL = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);        
        $remoteDomain = parse_url($finalURL, PHP_URL_HOST);
        
        curl_close($ch);
        
        /* Create a new DOM document parser */
        $html = new DOMDocument();
        $html->loadHTML($data);
        $xpath = new DOMXPath($html);
        
        /* If css checked, parse <link> tags and swap domains */
        if($css) {
            $css_tags = $xpath->query('//link/@href');
            foreach($css_tags as $css_tag) {
                $css_tag->value = str_replace($remoteDomain, $localDomain, $css_tag->value);
            }
        }
        
        /* If js checked, parse <script> tags and swap domains */
        if($js) {
            $js_tags = $xpath->query('//script/@src');
            foreach($js_tags as $js_tag) {
                $js_tag->value = str_replace($remoteDomain, $localDomain, $js_tag->value);
            }
        }

        $a_tags = $xpath->query('//a'); // get all <a> tags        
        foreach($a_tags as $a_tag) {
            /* Find all links that match the origin domain, or are relative links, and replace them with this scripts url format */
            if($a_tag->getAttribute('href') == '#') continue; // skip # anchors
            if(strpos($a_tag->getAttribute('href'), str_replace('www.', '', $remoteDomain)) !== FALSE) {
                /* Changing out absolute internal links */
                $origin = $a_tag->getAttribute('href');
                $a_tag->setAttribute('href', ('?css='.($css ? 1 : 0).'&js='.($js ? 1 : 0).'&localDomain='.$localDomain.'&remoteURL='.$origin));
            } elseif(substr($a_tag->getAttribute('href'), 0, 1) == '/') {
                /* Change out relative internal links */
                $origin = $a_tag->getAttribute('href');
                $a_tag->setAttribute('href', ('?css='.($css ? 1 : 0).'&js='.($js ? 1 : 0).'&localDomain='.$localDomain.'&remoteURL=http://'.$remoteDomain.$origin));
            }
        }

        /* Print modified output */
        echo $html->saveHTML();
        
        return;
    } else {
        if(!$remoteURL) $errors[] = 'Please enter a remote URL.';
        if(!$localDomain) $errors[] = 'Please enter a local domain.';
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Local Dev Swapper</title>
    
    <style type="text/css">
        form {
            margin: 10px;
            border: 1px solid #ccc;
            width: 60%;
            padding: 20px;
        }
        
        form div {
            margin: 10px 0;
        }
        
        form label {
            float: left;
            width: 150px;
            font-weight: bold;
        }
        
        form input.button {
            margin-left: 150px;
        }
        
        .errors {
            width: 50%;
            background: #fee5e2;
            color: #51241c;
            font-weight: 700;
            border: 1px solid #fdb3a8;
            margin: 10px 25px;
            padding: 10px;
            list-style: none;
        }
    </style>
</head>
<body>
    <h1>Local Dev Swapper Thingy</h1>
    <?php 
        if(!empty($_GET) && !empty($errors)) {
            echo '<ul class="errors">';
            foreach($errors as $error) {
                echo '<li>'.$error.'</li>';
            }
            echo '</ul>';
        }
    ?>
    <form>
        <div>
            <label>Remote URL</label>
            <input type="text" size="75" name="remoteURL" value="<?=$remoteURL?>" placeholder="http://teamddm.com/about">
        </div>
        <div>
            <label>Local Domain</label>
            <input type="text" size="40" name="localDomain" value="<?=$localDomain?>" placeholder="teamddm.lo">
        </div>
        <div>
            <label>Swap CSS</label>
            <input type="checkbox" name="css" value="1" <?=($css ? ' checked="checked" ' : '')?>>
        </div>
        <div>
            <label>Swap JS</label>
            <input type="checkbox" name="js" value="1" <?=($js ? ' checked="checked" ' : '')?>>
        </div>
        <div>
            <input type="submit" class="button" value="Submit">
        </div>
    </form>
</body>
</html>